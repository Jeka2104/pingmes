> Sum = fun() -> B = 2+3, io:format("B:~p~n",[B])  end.
> Y = fun() -> C = 1+1  end.

> pingmes:actions([{sum,Sum},{test2,Y}]).

> pingmes:message(sum).
> B:5
> ok
> pingmes:message(test2).
> ok

> pingmes:show_actions().
> [sum,test2,timeout]
