-module(pingmes).

-behaviour(gen_server).

%% API
-export([start_link/0]).
-export([actions/1, message/1, show_actions/0, del_actions/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {actions=[{timeout, fun()-> ok end}],
                timerRef}
        ).
%%%===================================================================
%%% API
%%%===================================================================


actions(List) when is_list(List) ->
    case valid_list(List) of
        true ->
            gen_server:call(?MODULE, {actions, List});
        {error_no_function} -> {error_no_function}
    end.

message(Msg) ->
    gen_server:cast(?MODULE, Msg).

show_actions() ->
    gen_server:call(?MODULE, {show_actions}).

del_actions(Msg) ->
    gen_server:call(?MODULE, {del_actions, Msg}).

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->
    {ok, #state{}}.

handle_call({actions, List}, _From, #state{actions = Actions, timerRef = TimerRefOld} = State) ->
    NewList = merge(List, Actions),
    TimerRef = timer_stop_start(TimerRefOld),
    {reply, ok, State#state{actions = NewList, timerRef = TimerRef}};

handle_call({show_actions}, _From, #state{actions = ActionsFull} = State) ->
    Actions = proplists:get_keys(ActionsFull),
    {reply, Actions, State};

handle_call({del_actions, Msg}, _From, #state{actions = Actions} = State) ->
    NewList = proplists:delete(Msg, Actions),
    {reply, ok, State#state{actions = NewList}}.

handle_cast(Msg, #state{timerRef = TimerRefOld, actions = Actions} = State) ->
    case check_actions(Msg, Actions) of
        undefined ->
            {noreply, State};
        _ ->
            TimerRef = timer_stop_start(TimerRefOld),
            {noreply, State#state{timerRef = TimerRef}}
    end.

handle_info({timeout, TimerRefOld, {error}}, #state{timerRef = TimerRefOld, actions = Actions} = State) ->
    Fun = proplists:get_value(timeout, Actions),
    spawn(Fun),
    TimerRef = timer_stop_start(TimerRefOld),
    {noreply, State#state{timerRef = TimerRef}};

handle_info(Msg, #state{timerRef = TimerRefOld, actions = Actions} = State) ->
    case check_actions(Msg, Actions) of
        undefined ->
            {noreply, State};
        _ ->
            TimerRef = timer_stop_start(TimerRefOld),
            {noreply, State#state{timerRef = TimerRef}}
    end.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

check_actions(Msg, Actions) ->
    case lists:keyfind(Msg, 1, Actions) of
        false ->
            undefined;
        {_K, V} ->
            spawn(V)
    end.

valid_list([{_, Fun} | T]) ->
    case is_function(Fun, 0) of
        false ->
            {error_no_function};
        true ->
            valid_list(T)
    end;

valid_list([]) ->
    true.

merge([{K, V} | T], Old) ->
    Old2 = proplists:delete(K, Old),
    merge(T, [{K, V} | Old2]);
merge([_ | T], Old) ->
    merge(T, Old);
merge([], New) ->
    New.

timer_stop_start(TimerRefOld) ->
    Time = application:get_env(pingmes, timer, 60000),
    case TimerRefOld of
        undefined ->
            TimerRef = erlang:start_timer(Time, self(), {error}),
            TimerRef;
        _ ->
            erlang:cancel_timer(TimerRefOld),
            TimerRef = erlang:start_timer(Time, self(), {error}),
            TimerRef
    end.


